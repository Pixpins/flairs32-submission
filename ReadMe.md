This project compiles the two experiments submitted to the FLAIRS32 conderence.

#1. DBPedia ontology

We analyze a database constructed using both the informations contained in the DBPedia ontology part dedicated to films and the IMDB website (www.imdb.com).

#2. PO2 ontology

We analyze a database constructed randomly generated using the PO2 (Process and Observation Ontology) ontology.

